package itis.parsing2;

import itis.parsing2.annotations.Concatenate;

import java.awt.image.AreaAveragingScaleFilter;
import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class FactoryParsingServiceImpl implements FactoryParsingService {

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException {
        List<File> files = parseDirect(factoryDataDirectoryPath);
        HashMap<String, String> nameAndValue = new HashMap<>();
        List<FactoryParsingException.FactoryValidationError> errors = new ArrayList<>();
        Class factoryClass = Factory.class;
        Constructor<? extends Factory> constructor = null;
        try {
            constructor = factoryClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            Factory factory = constructor.newInstance();
            Field[] fields = factoryClass.getDeclaredFields();
            ArrayList<String> fieldArrayList = new ArrayList<>();
            for (int i = 0; i < fields.length; i++) {
                fieldArrayList.add(fields[i].getName());
            }
            for (int i = 0; i < files.size(); i++) {
                FileReader fileReader = null;
                fileReader = new FileReader(files.get(i));
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                ArrayList<String> stringArrayList = new ArrayList<>();
                while (true) {
                    String string;
                    string = bufferedReader.readLine();
                    if (string == null) {
                        break;
                    }
                    stringArrayList.add(string);
                    String[] fieldsString = string.split(":");
                    if (fieldsString.length == 2) {
                        String name = fieldsString[0].replace("\"", "");
                        if (fieldArrayList.contains(name)) {
                            String notValidValue = fieldsString[1].replace("\"", "");
                            nameAndValue.put(name, notValidValue);
                        }
                    }
                }
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            for (int i = 0; i < fields.length; i++) {
                fields[i].setAccessible(true);
                if (fields[i].getName().equals("organizationChiefFullName")) {
                    fields[i].set(factory, nameAndValue.get("organizationChiefFullName"));
                }
                if (fields[i].getName().equals("title")) {
                    fields[i].set(factory, nameAndValue.get("title"));
                }
                if (fields[i].getName().equals("description")) {
                    fields[i].set(factory, nameAndValue.get("description"));
                    System.out.println(fields[i].getName());
                }
                if (fields[i].getName().equals("amountOfWorkers")) {
                    fields[i].setAccessible(true);
                    if (nameAndValue.get("amountOfWorkers") != null) {
                        fields[i].set(factory, Long.parseLong(nameAndValue.get("amountOfWorkers")));
                    }
                }
                if (fields[i].getName().equals("departments")) {
                    fields[i].setAccessible(true);
                    String value = nameAndValue.get("departments");
                    String validValue = value.replace("[", "").replace("]", "");
                    List<String> valuesOfField = Arrays.asList(validValue.split(",").clone());
                    fields[i].set(factory, valuesOfField);
                }
                try {
                    if (nameAndValue.get(fields[i]).equals("") || nameAndValue.get(fields[i]) == null) {

                        FactoryParsingException.FactoryValidationError factoryValidationError =
                                new FactoryParsingException.FactoryValidationError(fields[i].getName(),
                                        "значение = null");

                        errors.add(factoryValidationError);
                    }

                } catch (NullPointerException e) {
                    FactoryParsingException.FactoryValidationError factoryValidationError =
                            new FactoryParsingException.FactoryValidationError(fields[i].getName(),
                                    "значение = null");
                    errors.add(factoryValidationError);
                }
                for (Annotation annotation : fields[i].getDeclaredAnnotations()
                ) {
                    if (annotation instanceof Concatenate) {
                        String[] strings  = ((Concatenate) annotation).fieldNames();
                        StringBuilder newString = new StringBuilder();
                        for (int j = 0; j < strings.length; j++) {
                            if (j != strings.length - 1) {
                                newString.append(nameAndValue.get(strings[j])).append(fields[i].getAnnotation(Concatenate.class).delimiter());
                            } else {
                                newString.append(nameAndValue.get(strings[j]));

                            }
                        }
                        fields[i].set(factory, newString.toString());
                    }
                }
            }
            if(errors.size()!=0){
                throw new FactoryParsingException("Ошибка парсинга",errors);
            }
            return factory;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List parseDirect(String path ){
        File dir = new File(path);
        List<File> lst = new ArrayList<>();
        for ( File file : dir.listFiles() ){
            if ( file.isFile() )
                lst.add(file);
        }
        return lst;
    }
}
